import adapter from "@sveltejs/adapter-auto";
import sveltePreprocess from "svelte-preprocess";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: [
    sveltePreprocess({
      // sourceMap: !production,
      scss: {
        prependData: '@use "src/variables.scss" as *;',
      },
    }),
  ],
  kit: {
    adapter: adapter(),
  },
  css: (css) => {
    css.write("public/bundle.css");
  },
};

export default config;
