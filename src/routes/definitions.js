export const PAGE_LEN_OPTIONS = [5, 10, 20, 50, 80, 100];
export const PAGE_LEN_DEFAULT = 10;
