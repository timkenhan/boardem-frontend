import { writable } from "svelte/store";
import { PAGE_LEN_DEFAULT } from './definitions';

export const page_num = writable(0);
export const page_len = writable(PAGE_LEN_DEFAULT);
